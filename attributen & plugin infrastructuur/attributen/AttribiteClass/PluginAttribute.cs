﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttribiteClass
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PluginAttribute : Attribute
    {
        public string PluginOmschrijving { get; set; }
        public bool IsPlugin { get; set; }

        public PluginAttribute(string pluginOmschrijving, bool isPlugin)
        {
            this.PluginOmschrijving = pluginOmschrijving;
            this.IsPlugin = isPlugin;
        }
    }
}
