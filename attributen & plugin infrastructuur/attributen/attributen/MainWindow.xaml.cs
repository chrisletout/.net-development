﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AttribiteClass;

namespace attributen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ReadAssembliesName();
        }

        private void ReadAssembliesName()
        {
            //string path = AppDomain.CurrentDomain.BaseDirectory + "subplugins";
            var files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + "subplugins", "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".dll") || s.EndsWith(".exe")); ;
            foreach (string file in files)
            {
                LoadAssemblies(file);
            }
        }

        private void LoadAssemblies(string sAssembly)
        {
            Assembly ass = Assembly.LoadFile(sAssembly);
            foreach (Type t in ass.GetTypes())
            {
                string desc = PluginDescription(t);
                if (desc != null)
                {
                    menu.Items.Add(new PluginItem(desc, t));
                }
            }
        }

        /// <summary>
        /// looks for the plugin description of this type, returns null if not a plugin (or the description is null)
        /// </summary>
        /// <param name="t">the type which is beeing searched to be a plugin etc etc</param>
        /// <returns>the description if plugin, null else </returns>
        private String PluginDescription(Type t)
        {
            IEnumerable<Attribute> attrs = t.GetCustomAttributes();
            foreach (Attribute attr in attrs)
                if (attr.GetType().Equals((typeof(PluginAttribute))))
                    if ((attr as PluginAttribute).IsPlugin)
                        return (attr as PluginAttribute).PluginOmschrijving;

            return null;
        }


        //private void verwerkDemoUI()
        //{
        //    Microsoft.Win32.OpenFileDialog ofd = new Microsoft.Win32.OpenFileDialog();
        //    ofd.Filter = ".NET assemblies|*.exe;*dll";
        //    ofd.InitialDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
        //    if (ofd.ShowDialog().Value)
        //    {
        //        verwerkDemoUI(ofd.FileName);
        //    }


        //}
        //private void verwerkDemoUI(string sAssembly)
        //{
        //    Assembly ass = Assembly.LoadFile(sAssembly);
        //    foreach (Type t in ass.GetTypes())
        //    {
        //        Console.WriteLine(t.Name);
        //        verwerkIndienWindow(t);
        //        verwerkIndienPersoon(t);
        //    }
        //}

        //private void verwerkIndienWindow(Type t)
        //{
        //    if (t.Name.Equals("MainWindow"))
        //    {
        //        Window wdw = (Window) Activator.CreateInstance(t);
        //        wdw.Show();
        //    }
        //}

        //private void verwerkIndienPersoon(Type t)
        //{
        //    if (t.Name.Equals("clsPersoon"))
        //    {
        //        Object p = Activator.CreateInstance(t);
        //        t.InvokeMember("Firstname", BindingFlags.SetProperty, null, p, new object[] {"Familie"});
        //    }
        //}
    }
}
