﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using AttribiteClass;

namespace attributen
{
    public class PluginItem : MenuItem
    {
        public Type type { get; set; }

        public PluginItem(string desc, Type t)
        {
            this.type = t;
            Header = desc;
            Click += new System.Windows.RoutedEventHandler(this.menuItem1_Click);
        }

        private void menuItem1_Click(object sender, System.EventArgs e)
        {
            Window wdw = (Window) Activator.CreateInstance(type);
            wdw.Show();
        }
    }
}
