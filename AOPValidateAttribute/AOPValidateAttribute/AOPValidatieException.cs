﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOPValidateAttribute
{
    public class AOPValidatieException : Exception
    {
        private int minimum;
        private AOPValidate aOPValidate;

        public AOPValidatieException(string message, Exception innerException) : base(message, innerException)
        {
            Console.WriteLine(message);
            Console.WriteLine(innerException);
        }

        public AOPValidatieException(string message) : base(message)
        {
            Console.WriteLine(message);
        }

        public AOPValidatieException(string message, int minimum, AOPValidate aOPValidate) : base(message)
        {
            this.minimum = minimum;
            this.aOPValidate = aOPValidate;
            Console.WriteLine(message);
        }
    }
}
