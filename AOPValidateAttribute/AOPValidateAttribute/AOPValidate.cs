﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Aspects;

namespace AOPValidateAttribute
{
    [Serializable]
    public class AOPValidate : PostSharp.Aspects.OnMethodBoundaryAspect
    {
        private ParameterInfo[] formeleParameter;
        public int Minimum { get; set; }

        public int Maximum
        {
            get { return _maximum; }
            set { _maximum = DateTime.Today.Year; }
        }

        private int _maximum;

        public override void OnEntry(MethodExecutionArgs args)
        {
            base.OnEntry(args);
            formeleParameter = args.Method.GetParameters();
            CheckParameterPrecondities(args);

        }

        private void CheckParameterPrecondities(MethodExecutionArgs args)
        {
            for (var i = 0; i < formeleParameter.Length; i++)
                CheckParameterPrecondities(args.Arguments[i], formeleParameter[i]);
        }

        private void CheckParameterPrecondities(object args, ParameterInfo parameterInfoformele)
        {
            foreach (GeboorteRangeAttribute geboorteRange in parameterInfoformele.GetCustomAttributes<GeboorteRangeAttribute>())
                geboorteRange.Valideer(args);
        }

        //public void Valideer(object o)
        //{
        //    int value = (int) o;
        //    if (value < 1900) throw new AOPValidatieException("Geboortejaar is te klein", this.Minimum, this);
        //    if(value > Maximum) throw new AOPValidatieException("Geboortejaar is te groot", this.Minimum, this);
        //}
    }
}
