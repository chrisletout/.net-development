﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOPValidateAttribute
{
    public class Persoon
    {
        public string Naam { get; set; }
        public int GeboorteJaar { get; set; }
        public int Rijksregisternummer { get; set; }

        public Persoon(string naam, int geboortedatum, int rijksregisternummer)
        {
            Naam = naam;
            GeboorteJaar = geboortedatum;
            Rijksregisternummer = rijksregisternummer;
        }
    }
}
