﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOPValidateAttribute
{
    public interface IAOPValideer
    {
        void Valideer(Object o);
    }
}
