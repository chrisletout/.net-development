﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOPValidateAttribute
{
    [AttributeUsage(AttributeTargets.All)]
    public class GeboorteRangeAttribute : Attribute, IAOPValideer
    {
        public int Mimimum { get; set; }

        public GeboorteRangeAttribute(int mimimum)
        {
            Mimimum = mimimum;
        }

        public void Valideer(object o)
        {
            int value = (int)o;
            if (value < Mimimum) throw new AOPValidatieException("Geboortejaar is te klein");
            if (value > DateTime.Today.Year) throw new AOPValidatieException("Geboortejaar is te groot");
        }
    }
}
