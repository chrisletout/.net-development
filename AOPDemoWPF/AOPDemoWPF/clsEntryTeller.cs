﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PostSharp.Aspects;

namespace AOPDemoWPF
{
    [Serializable]
    public class clsEntryTeller : PostSharp.Aspects.OnMethodBoundaryAspect
    {
        private static Dictionary<string, int> _telDictionary = new Dictionary<string, int>(); 
        public override void OnEntry(MethodExecutionArgs args)
        {
            base.OnEntry(args);
            string MethodFullName = args.Method.DeclaringType.FullName + "." + args.Method.Name;

            if (_telDictionary.Keys.Contains(MethodFullName))
            {
                int already = _telDictionary[MethodFullName];
                _telDictionary.Remove(MethodFullName);
                already += 1;
                _telDictionary.Add(MethodFullName, already);
            }
            else
            {
                _telDictionary.Add(MethodFullName, 1);
            }
        }

        public static void DumpInfo()
        {
            foreach (string k in _telDictionary.Keys)
            {
                int t = _telDictionary[k];
                Console.WriteLine(t);
            }
        } 
    }
}
