﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOPDemoWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            btnstart.Click += BtnstartOnClick;
        }

        private void BtnstartOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            TestAOSP();
        }

        private void TestAOSP()
        {
            Wiskundige ivo  = new Wiskundige();
            int n = 0;
            if (int.TryParse(TextBoxN.Text, out n))
            {
                int f = ivo.Fibonacci(n);
                TextBlockResult.Text = f.ToString();
            }
        }
    }
}
