﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace dragbehaviorsinblend
{
    public class ResizeBehavior : Behavior<UIElement>
    {
        private AdornerLayer adornerLayer;
          private static Window parent;
          private FrameworkElement fe;
          private UIElement attachedElement;



        static ResizeBehavior()
        {
             parent = Application.Current.MainWindow;
        }





        protected override void OnAttached()
          {
              attachedElement = this.AssociatedObject;
              fe = attachedElement as FrameworkElement;
   
             if (fe.Parent != null)
            {
                (fe.Parent as FrameworkElement).Loaded += ResizeBehaviorParent_Loaded;
              }
          }
  
          protected override void OnDetaching()
          {
              base.OnDetaching();
              if (adornerLayer != null)
              {
                  adornerLayer = null;
              }
          }

        private void ResizeBehaviorParent_Loaded(object sender, RoutedEventArgs e)
        {
              if (adornerLayer == null)
                  adornerLayer = AdornerLayer.GetAdornerLayer(sender as Visual);
           attachedElement.MouseEnter += AttachedElement_MouseEnter;
        }
        private void AttachedElement_MouseEnter(object sender, MouseEventArgs e)
          {
             // ResizingAdorner resizingAdorner = new ResizingAdorner(sender as UIElement);
             // resizingAdorner.MouseLeave += ResizingAdorner_MouseLeave;
             //adornerLayer.Add(resizingAdorner);
          }


        private void ResizingAdorner_MouseLeave(object sender, MouseEventArgs e)
         {
              if (sender != null)
              {
                 //adornerLayer.Remove(sender as ResizingAdorner);
              }
          }
}
}
