﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Label = System.Windows.Controls.Label;

namespace dragBahaviors
{
    public static class DragDropBehavior
    {
        private static Point _startPosition;
        private static Point _mouseStartPosition;
        private static TranslateTransform _translatetransform;
        private static UIElement _associatedObject = null;
        private static Window _parent = null;
        public static readonly DependencyProperty DragDropProperty =
            DependencyProperty.RegisterAttached
            (
                "DragDrop",
                typeof(bool),
                typeof(DragDropBehavior),
                new UIPropertyMetadata(false, OnDragDrop)
            );
        public static bool GetSelectAllOnFocus(DependencyObject obj)
        {
            return (bool)obj.GetValue(DragDropProperty);
        }
        public static void SetSelectAllOnFocus(DependencyObject obj, bool value)
        {
            obj.SetValue(DragDropProperty, value);
        }
        private static void OnDragDrop(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //System.Windows.Controls.Label textBox = d as System.Windows.Controls.Label;
            var uiElement = d as UIElement;
            if (uiElement != null)
            {
                _associatedObject = uiElement;
                _parent = Application.Current.MainWindow;
                _translatetransform = new TranslateTransform();
                _associatedObject.RenderTransform = _translatetransform;
                _associatedObject.MouseLeftButtonDown += OnMouseLeftButtonDown;
                _associatedObject.MouseLeftButtonUp += OntMouseLeftButtonUp;
                _associatedObject.MouseMove += OnMouseMove;
            }
        }

        private static void OnMouseMove(object sender, MouseEventArgs e)
        {
            //Vector positionDifference = e.GetPosition(_parent) – _mouseStartPosition;
            double x = e.GetPosition(_parent).X;
            double xx = _mouseStartPosition.X;
            double y = e.GetPosition(_parent).Y;
            double yy = _mouseStartPosition.Y;

            double differenceX = x - xx;
            double differenceY = y - yy;

            if (_associatedObject.IsMouseCaptured)
            {
                _translatetransform.X = _startPosition.X +  differenceX;
                _translatetransform.Y = _startPosition.Y + differenceY;
            }
        }

        private static void OntMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _associatedObject.ReleaseMouseCapture();
        }

        private static void OnMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _startPosition = _associatedObject.TranslatePoint(new Point(), _parent);
            _mouseStartPosition = e.GetPosition(_parent);
            _associatedObject.CaptureMouse();
        }
    }
}
