﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinApp.Model
{
    public class Observation : ObservableObject
    {
        private String _startTime;
        private String _endTime;

        public String StartTime
        {
            get { return _startTime; }
            set { _startTime = value; OnPropertyChanged("StartTime"); }
        }

        public String EndTime
        {
            get { return _endTime; }
            set { _endTime = value; OnPropertyChanged("EndTime");}
        }

        public Observation(String startTime, String endTime)
        {
            _startTime = startTime;
            _endTime = endTime;
        }
    }
}
