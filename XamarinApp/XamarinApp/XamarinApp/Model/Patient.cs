﻿using System;
using XamarinApp.Model;

namespace XamarinApp
{
	public class Patient : ObservableObject
    {
	    private String _firstName;
	    private String _lastName;
		public Patient ()
		{
		}

	    public Patient(string firstName, string lastName)
	    {
	        _firstName = firstName;
	        _lastName = lastName;
	    }

	    public string FirstName
	    {
	        get { return _firstName; }
	        set { _firstName = value; }
	    }

	    public string LastName
	    {
	        get { return _lastName; }
	        set { _lastName = value; }
	    }
	}
}

