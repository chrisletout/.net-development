﻿using System;
using XamarinApp.Model;

namespace XamarinApp
{
	public class Test : ObservableObject
    {
	    private String _name;
	    private int _observationSize;
		public Test ()
		{
		}

	    public Test(string name, int observationSize)
	    {
	        _name = name;
	        _observationSize = observationSize;
	    }

	    public string Name
	    {
	        get { return _name; }
	        set { _name = value; }
	    }

	    public int ObservationSize
	    {
	        get { return _observationSize; }
	        set { _observationSize = value; }
	    }

	    public override string ToString()
	    {
	        return Name +" "+ObservationSize;
	    }
    }
}

