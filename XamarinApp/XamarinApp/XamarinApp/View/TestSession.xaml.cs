﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using XamarinApp.ViewModel;

namespace XamarinApp
{
	public partial class TestSession : ContentPage
	{
        private TestSessionViewModel vm;
        public int Z { get; set; }
	    public int position = -1;
        public TestSession ()
		{
            vm = new TestSessionViewModel();
            BindingContext = vm;
            InitializeComponent ();
		}

	    public TestSession(Test test, Patient patients)
	    {
            vm = new TestSessionViewModel(test.ObservationSize);
            BindingContext = vm;
            Z = 0;
            InitializeComponent();
            LabelSelectedPatient.Text = patients.FirstName + " " + patients.LastName;
            LabelSelectedTest.Text = test.Name;
        }
        private bool cdn()
        {
            Z++;
            LabelTimer.Text = "Je ben al : "+Z.ToString()+ " seconden bezig";
            if (position < vm.ObservationsCollection.Count)
                return true;
            else
                return false;
        }

	    private void StartOservatie(object sender, EventArgs e)
	    {
            if(Z == 0)
                Device.StartTimer(new TimeSpan(0, 0, 1), cdn);
	        position ++;
            if(position < vm.ObservationsCollection.Count) {
                TestListView.SelectedItem = vm.ObservationsCollection[position];
                vm.ObservationsCollection[position].StartTime ="Observatie gestart om : " + DateTime.Now.ToString("h:mm:ss tt");
            }
            if (position > 0 && position <= vm.ObservationsCollection.Count)
                vm.ObservationsCollection[position - 1].EndTime = "Observatie gestopt om : " + DateTime.Now.ToString("h:mm:ss tt");
               
        }
	}
}

