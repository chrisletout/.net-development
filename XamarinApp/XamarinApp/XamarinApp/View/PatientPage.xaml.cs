﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace XamarinApp
{
	public partial class PatientPage : ContentPage
	{
	    public PatientPageViewModel Vm;
		public PatientPage ()
		{
            Vm = new PatientPageViewModel();
		    BindingContext = Vm;
			InitializeComponent();
		}

	    private void OnPatientSelected(object sender, ItemTappedEventArgs e)
	    {
            Patient patient = e.Item as Patient;
            Navigation.PushModalAsync(new NavigationPage(new TestPage(patient)));
        }
	}
}

