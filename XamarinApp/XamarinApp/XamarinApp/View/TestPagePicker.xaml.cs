﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace XamarinApp
{
    public partial class TestPagePicker : ContentPage
    {
        private Patient Patients;
        private TestPageViewModel vm;

        public TestPagePicker(Patient patient)
        {
            InitializeComponent();
            vm = new TestPageViewModel();
            BindingContext = vm;
            Patients = patient;
            
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            TestPageViewModel vmm = BindingContext as TestPageViewModel;

            if (vmm != null)
            {
                ThePicker.Items.Clear();
                foreach (var color in vmm.TestsList)
                {
                    ThePicker.Items.Add(color.ToString());
                }
            }
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            int i = vm.SelectedIndex;
            int ij = vm.SelectedIndex;
            //throw new NotImplementedException();
        }
    }
}

