﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace XamarinApp
{
	public partial class TestPage : ContentPage
	{
	    private Patient Patients;
	    private TestPageViewModel vm;
		public TestPage (Patient patient)
		{
            vm = new TestPageViewModel();
		    BindingContext = vm;
		    Patients = patient;
			InitializeComponent ();
		}

	    private void OnTestSelected(object sender, ItemTappedEventArgs e)
	    {
	        Test test = e.Item as Test;
	        Navigation.PushModalAsync(new NavigationPage(new TestSession(test, Patients)));
	    }
	}
}

