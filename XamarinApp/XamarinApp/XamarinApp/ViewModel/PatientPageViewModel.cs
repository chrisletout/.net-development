﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace XamarinApp
{
	public class PatientPageViewModel
	{
	    public ObservableCollection<Patient> PatientsList { get; set; }

	    public PatientPageViewModel ()
		{
            PatientsList = new ObservableCollection<Patient>();
            PatientsList.Add(new Patient("Gregg", "Globe"));
            PatientsList.Add(new Patient("Some", "Thing"));
            PatientsList.Add(new Patient("test1", "test1"));
            PatientsList.Add(new Patient("test2", "test2"));
            PatientsList.Add(new Patient("test3", "test3"));
            PatientsList.Add(new Patient("test4", "test4"));
		}
	}
}

