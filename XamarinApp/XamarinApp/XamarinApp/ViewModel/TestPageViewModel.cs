﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace XamarinApp
{
	public class TestPageViewModel
	{
	    public ObservableCollection<Test> TestsList { get; set; }
        public int SelectedIndex { get; set; }
	    public TestPageViewModel ()
		{
            TestsList = new ObservableCollection<Test>();
            TestsList.Add(new Test("eerste test", 5));
            TestsList.Add(new Test("tweede test", 4));
            TestsList.Add(new Test("derde test", 8));
            TestsList.Add(new Test("vierde test", 10));
            TestsList.Add(new Test("vijfde test", 12));
		}
	}
}

