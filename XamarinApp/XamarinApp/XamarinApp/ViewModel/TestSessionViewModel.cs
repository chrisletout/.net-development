﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using XamarinApp.Model;

namespace XamarinApp.ViewModel
{
    public class TestSessionViewModel : ObservableObject
    {
        private int observationSize;
        private ObservableCollection<Observation> _observationsCollection;
        public ObservableCollection<Observation> ObservationsCollection
        {
            get { return _observationsCollection; }
            set
            {
                _observationsCollection = value;
                OnPropertyChanged("ObservationsCollection");
            }
        }

        public TestSessionViewModel()
        {
            ObservationsCollection = new ObservableCollection<Observation>();
        }

        public TestSessionViewModel(int observationSize)
        {
            ObservationsCollection = new ObservableCollection<Observation>();
            this.observationSize = observationSize;
            for (int i = 0; i < observationSize; i++)
            {
                ObservationsCollection.Add(new Observation("Observatie is nog niet gestart",""));
            }
        }
    }
}
