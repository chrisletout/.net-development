﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace N_Queens
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool[,] bord = new bool[8, 8];
        public MainWindow()
        {
            InitializeComponent();
            
             NonQueenOk(7,0, bord);
            MaakBordGUI(bord);
        }

        private void MaakBordGUI(bool[,] bord)
        {
            //uniformgrid.RowDefinitions = (int)Math.Sqrt(bord.Length);
            //uniformgrid.Rows = (int)Math.Sqrt(bord.Length);

            

            for (int i = 0; i <= 7; i++)
            {
                uniformgrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star)});
                uniformgrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                for (int j = 0; j <= 7; j++)
                {
                    //Grid.SetRow(rect, 0);
                    //Grid.SetColumn(rect, 0);

                    //uniformgrid.Add(rect);
                    if (bord[i, j])
                    {
                        Rectangle rect = new Rectangle
                        {
                            Height = 25,
                            Width = 25,
                            Fill = new SolidColorBrush(Colors.Black)
                        };
                        rect.SetValue(Grid.RowProperty, i);
                        rect.SetValue(Grid.ColumnProperty, j);

                        uniformgrid.Children.Add(rect);
                    }
                }
            }
        }

        public static void setN(DependencyProperty obj, int value)
        {

        }

        public static Boolean NonQueenOk(int N, int stap, Boolean[,] bord)
        {
            if (stap >= N)
                return true;
            for (int kol = 0; kol < N; kol++)
            {
                bord[stap, kol] = true; //zet de koninging
                if (BordIsOk(bord, stap, kol))
                    if (NonQueenOk(N, stap + 1, bord))
                        return true;
                bord[stap, kol] = false; //verwijder  Q

            }
            return false;
        }

        private static bool BordIsOk(bool[,] bord, int rij, int kol)
        {
            
            if (!HorizontalOk(bord, rij, kol))
                return false;
            if (!VertikaalOk(bord, rij, kol))
                return false;
            //if (NotDiagonalOk(bord, rij, kol))
            //    return false;
            return true;

        }

        private static bool NotDiagonalOk(bool[,] bord, int rij, int kol)
        {
            for (int diagonaaltest = 0; diagonaaltest < rij; diagonaaltest++)
            {
                if (bord[rij + diagonaaltest, kol + diagonaaltest])
                {
                    return true;
                }
            }
            return false;
        }

        private static bool HorizontalOk(bool[,] bord, int rij, int kol)
        {
            for (int koltest = 0; koltest < kol; koltest++)
            {
                if (bord[rij, koltest])
                     return false;
            }
            return true;
        }

        private static bool VertikaalOk(bool[,] bord, int rij, int kol)
        {
            for (int rijtests = 1; rijtests < rij; rijtests++)
            {
                if (bord[rijtests, kol])
                    return false;
            }
            return true;
        }

        private Boolean DiagonalRightOK(Boolean[,] bord, int step, int kol)
        {
            for (int i = 1; i <= step; i++)
            {
                int rijtest = step - i,
                    koltest = kol + i;

                if (koltest >= 7)
                {
                    return true;
                }
                if (bord[rijtest, koltest])
                {
                    return false;
                }
            }

            return true;
        }

        private Boolean DiagonalLeftOK(Boolean[,] bord, int step, int kol)
        {
            for (int i = 1; i < step + 1; i++)
            {
                int rijtest = step - i,
                    koltest = kol - i;

                if (koltest < 0)
                {

                    return true;
                }
                if (bord[rijtest, koltest])
                {
                    return false;
                }
            }

            return true;
        }

    }
}

