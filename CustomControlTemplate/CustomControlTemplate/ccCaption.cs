﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CustomControlTemplate
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CustomControlTemplate"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:CustomControlTemplate;assembly=CustomControlTemplate"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:ccCaption/>
    ///
    /// </summary>
    [TemplatePart(Name = "PART_Minimize", Type = typeof(UIElement))]
    [TemplatePart(Name = "PART_Maximize", Type = typeof(UIElement))]
    [TemplatePart(Name = "PART_Close", Type = typeof(UIElement))]
    public class ccCaption : Control
    {
        UIElement minimizeUiElement;
        static ccCaption()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ccCaption), new FrameworkPropertyMetadata(typeof(ccCaption)));
        }

        public override void OnApplyTemplate()
        {
            
            minimizeUiElement = GetTemplateChild("PART_Minimize") as UIElement;
            UIElement maximizeUiElement = GetTemplateChild("PART_Maximize") as UIElement;
            UIElement closeUiElement = GetTemplateChild("PART_Close") as UIElement;
            minimizeUiElement.AddHandler(MouseUpEvent, new MouseButtonEventHandler((sender, args) => minimizeFunction(sender,args)),true);
            maximizeUiElement.AddHandler(MouseUpEvent, new MouseButtonEventHandler((sender, args) => maximizeFunction(sender,args)),true);
            closeUiElement.AddHandler(MouseUpEvent, new MouseButtonEventHandler((sender, args) => closeFunction(sender,args)),true);
            //minimizeUiElement.MouseUp += minimizeFunction;
            //maximizeUiElement.MouseUp += maximizeFunction;
            //closeUiElement.MouseUp += closeFunction;
            //base.OnApplyTemplate();
        }

        private void closeFunction(object sender, MouseButtonEventArgs e)
        {
            Window.GetWindow(minimizeUiElement).Close();
        }

        private void maximizeFunction(object sender, MouseButtonEventArgs e)
        {
            Window.GetWindow(minimizeUiElement).WindowState = WindowState.Maximized;
        }

        private void minimizeFunction(object sender, MouseButtonEventArgs e)
        {
           Window.GetWindow(minimizeUiElement).WindowState = WindowState.Minimized;
        }
    }
}
