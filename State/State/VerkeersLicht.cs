﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace State
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:State"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:State;assembly=State"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:VerkeersLicht/>
    ///
    /// </summary>
    [TemplateVisualState(Name = "RedState", GroupName = "VerkeersState")]
    [TemplateVisualState(Name = "GreenState", GroupName = "VerkeersState")]
    [TemplateVisualState(Name = "OrangeState", GroupName = "VerkeersState")]
    [TemplatePart(Name = "PART_RedElippse", Type = typeof(UIElement))]
    [TemplatePart(Name = "PART_OrangeElippse", Type = typeof(UIElement))]
    [TemplatePart(Name = "PART_GreenElippse", Type = typeof(UIElement))]
    public class VerkeersLicht : Control
    {
        static VerkeersLicht()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(VerkeersLicht), new FrameworkPropertyMetadata(typeof(VerkeersLicht)));
        }

        public override void OnApplyTemplate()
        {
            UIElement redEllipse = GetTemplateChild("PART_RedElippse") as UIElement;
            UIElement orangeEllipse = GetTemplateChild("PART_OrangeElippse") as UIElement;
            UIElement greenEllipse = GetTemplateChild("PART_GreenElippse") as UIElement;

            redEllipse.AddHandler(MouseUpEvent, new MouseButtonEventHandler(GoToStateRed));
            orangeEllipse.AddHandler(MouseUpEvent, new MouseButtonEventHandler(GoToStateOrange));
            greenEllipse.AddHandler(MouseUpEvent, new MouseButtonEventHandler(GoToStateGreen));
        }

        private void GoToStateGreen(object sender, MouseButtonEventArgs e)
        {
            VisualStateManager.GoToState(this, "GreenState", false);
        }

        private void GoToStateOrange(object sender, MouseButtonEventArgs e)
        {
            VisualStateManager.GoToState(this, "OrangeState", false);
        }

        private void GoToStateRed(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            VisualStateManager.GoToState(this, "RedState", false);
            //VisualStateManager.GoToElementState(this as FrameworkElement, "RedState", true);
        }
    }
}
