﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver
{
    public class Solver
    {
        public int[,] bord = new int[9, 9];
        
        public Solver()
        {
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    bord[i, j] = -1;
                }
            }
            LosSudokuOp(new Zet(){Kolom = 0, Rij = 0}, bord);
        }

        public bool LosSudokuOp(Zet zet, int[,] bord)
        {
            if (BuitenBord(zet, bord)) return true;
            for (int get = 1; get <= 9; get++)
            {
                ZetOpBord(bord, zet, get);
                if (GoedBord(bord, zet))
                {
                    int kol = zet.Kolom;
                    if (zet.Kolom == 8)
                    {
                        zet.Rij = zet.Rij + 1;
                        zet.Kolom = -1;
                    }
                    if (LosSudokuOp(new Zet() {Rij = zet.Rij, Kolom = (zet.Kolom + 1)}, bord))
                        return true;
                    zet.Kolom = kol;
                }
            }
            VerwijderVanBord(bord, zet);
            return false;
        }

        private bool GoedBord(int[,] bord, Zet zet)
        {
            List<int> rijenList = new List<int>();
            List<int> kolomList = new List<int>();
            for (int i = 0; i < 9; i++)
            {
                rijenList.Add(bord[i, zet.Kolom]);
                kolomList.Add(bord[zet.Rij, i]);
            }
            if (HasDuplicates(rijenList)) return false;
            if (HasDuplicates(kolomList)) return false;

            if (ControlePerBlokjeVanNegenNietInOrder(bord, zet)) return false;

            return true;
        }

        private bool ControlePerBlokjeVanNegenNietInOrder(int[,] bord, Zet zet)
        {
            int rij = 0, kolom = 0;
            List<int> rijenList = new List<int>();
            if (zet.Rij < 3)
                rij = 0;
           else if (zet.Rij < 6)
                rij = 3;
           else if (zet.Rij < 9)
               rij = 6;

            if (zet.Kolom < 3)
                kolom = 0;
            else if (zet.Kolom < 6)
                kolom = 3;
            else if (zet.Kolom < 9)
                kolom = 6;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    rijenList.Add(bord[i + rij,j + kolom]);
                }
            }

            if (HasDuplicates(rijenList)) return true;

            return false;
        }

        public bool HasDuplicates(List<int> myList)
        {
            var hs = new HashSet<int>();

            for (var i = 0; i < myList.Count; ++i)
            {
                if(myList[i] > 0)
                    if (!hs.Add(myList[i])) return true;
            }
            return false;
        }
        private void ZetOpBord(int[,] bord, Zet zet, int getal)
        {
            //if(bord[zet.Rij, zet.Kolom] <= 0)
                bord[zet.Rij, zet.Kolom] = getal;
        }
        private void VerwijderVanBord(int[,] bord, Zet zet)
        {
            bord[zet.Rij, zet.Kolom] = -1;
        }

        

        private bool BuitenBord(Zet zet, int[,] bord)
        {
            //indien cell buiten het bord valt zijn alle velden ingevuld en hebben we een oplossing in bord;
            try
            {
                int temp = bord[zet.Rij, zet.Kolom];
                bord[zet.Rij, zet.Kolom] = temp;
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex);
                return true;
            }
            return false;
        }
    }
}
