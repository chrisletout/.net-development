﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver
{
    public class Zet
    {
        private int _rij;
        private int _kolom;

        public int Rij
        {
            get { return _rij; }
            set { _rij = value; }
        }

        public int Kolom
        {
            get { return _kolom; }
            set { _kolom = value; }
        }
    }
}
