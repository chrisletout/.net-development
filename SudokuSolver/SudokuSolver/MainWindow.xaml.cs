﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SudokuSolver
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Solver s = new Solver();
            MaakBordGUI(s.bord);
        }

        private void MaakBordGUI(int[,] bord)
        {
            //uniformgrid.RowDefinitions = (int)Math.Sqrt(bord.Length);
            //uniformgrid.Rows = (int)Math.Sqrt(bord.Length);



            //for (int i = 0; i <= 8; i++)
            //{
                //uniformgrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
                //uniformgrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
                //for (int j = 0; j <= 8; j++)
                //{
                //    //Grid.SetRow(rect, 0);
                //    //Grid.SetColumn(rect, 0);

                //    //uniformgrid.Add(rect);
                //    if (bord[i, j])
                //    {
                //        Rectangle rect = new Rectangle
                //        {
                //            Height = 25,
                //            Width = 25,
                //            Fill = new SolidColorBrush(Colors.Black)
                //        };
                //        rect.SetValue(Grid.RowProperty, i);
                //        rect.SetValue(Grid.ColumnProperty, j);

                //        uniformgrid.Children.Add(rect);
                //    }
                //}
                for (int j = 0; j < 9; j++)
                {
                    for (int k = 0; k < 9; k++)
                    {
                        UniformGrid.Children.Add(new TextBlock { Text = bord[j,k].ToString() });
                    }
                }
            //}
        }
    }
}
