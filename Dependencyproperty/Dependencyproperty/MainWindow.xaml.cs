﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dependencyproperty
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            AnimeerBreedte();
            AnimeerBackground();
            Transformeer();
        }
        private void AnimeerBreedte()
        {
            DoubleAnimation anWidth = new DoubleAnimation(BtnButton.Width, BtnButton.Width *1.1, new Duration(new TimeSpan(0,0,2)));
            anWidth.SetValue(Storyboard.TargetNameProperty, BtnButton.Name);
            anWidth.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath(Button.WidthProperty));
            anWidth.AutoReverse = true;
            anWidth.RepeatBehavior = RepeatBehavior.Forever;

            Storyboard sb = new Storyboard();
            sb.Children.Add(anWidth);
            sb.Begin(this);
        }

        private void AnimeerBackground()
        {
            ColorAnimation anColorAnimation = new ColorAnimation(Colors.Red, new Duration(new TimeSpan(0,2,2)));
            Storyboard.SetTargetName(anColorAnimation,BtnButton.Name);
            Storyboard.SetTargetProperty(anColorAnimation, new PropertyPath("(Panel.Background).(SolidColorBrush.Color)"));
            anColorAnimation.AutoReverse = true;
            anColorAnimation.RepeatBehavior = RepeatBehavior.Forever;
            Storyboard sb = new Storyboard();
            sb.Children.Add(anColorAnimation);
            sb.Begin(this);
        }

        private void Transformeer()
        {
            ScaleTransform scale = new ScaleTransform(0.75,1.5);
            RotateTransform rotate = new RotateTransform(90);
            //SkewTransform skewTransform = new SkewTransform(30, 30);
            TransformGroup transformGroup = new TransformGroup();

            transformGroup.Children.Add(scale);
            transformGroup.Children.Add(rotate);
            //transformGroup.Children.Add(skewTransform);

            BtnButton.RenderTransform = transformGroup;

            this.RegisterName("myrot", rotate);
            this.RegisterName("myscale", scale);


            DoubleAnimation anXScaleAnimation = new DoubleAnimation(1, 1.5, new Duration(new TimeSpan(0,0,0,01)));
            anXScaleAnimation.SetValue(Storyboard.TargetNameProperty, "myscale");
            anXScaleAnimation.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath(ScaleTransform.ScaleXProperty));
            anXScaleAnimation.RepeatBehavior = RepeatBehavior.Forever;

            DoubleAnimation anYScaleAnimation = new DoubleAnimation(1, 2, new Duration(new TimeSpan(0, 0, 0, 01)));
            anYScaleAnimation.SetValue(Storyboard.TargetNameProperty, "myscale");
            anYScaleAnimation.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath(ScaleTransform.ScaleYProperty));
            anYScaleAnimation.RepeatBehavior = RepeatBehavior.Forever;



            DoubleAnimation anAngle = new DoubleAnimation(rotate.Angle, rotate.Angle + 360, new Duration(new TimeSpan(0, 0, 4)));
            anAngle.SetValue(Storyboard.TargetNameProperty, "myrot");
            anAngle.SetValue(Storyboard.TargetPropertyProperty, new PropertyPath(RotateTransform.AngleProperty));
            anAngle.RepeatBehavior = RepeatBehavior.Forever;

            Storyboard sb = new Storyboard();
            sb.Children.Add(anAngle);
            sb.Children.Add(anXScaleAnimation);
            sb.Children.Add(anYScaleAnimation);
            sb.Begin(this);
        }
    }
}
